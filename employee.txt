using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    class Employee extends Person
    {
        private int id;
        private String firstName;
        private String lastName;
        private int departmentId;
        private double salary;
        private String gender;
        private int supervisor;

        public Employee(int id, string fn, string ln, int depId, double sal, string gend, int sup)
        {
            super(id, ln, fn, gend);
            departmentId = depId;
            salary = sal;
            supervisor = sup;
        }

        public int getDepartmentId()
        {
            return departmentId;
        }
        public double getSalary()
        {
            return salary;
        }
        public string getGender()
        {
            return gender;
        }
        public int getSupervisor()
        {
            return supervisor;
        }
    }
}
