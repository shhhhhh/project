﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Backend
{
    public class Product
    {
        private int productId = 0;
        private string name;
        private string type;
        private int location;
        private bool inStock;
        private int stockCount;
        private double price;

        public Product(string _name, string _type, int _productid, int _location, bool _inStock, int _stockCount, double _price)
        {
            name = _name;
            type = _type;
            productId = _productid;
            location = _location;
            inStock = _inStock;
            stockCount = _stockCount;
            price = _price;
        }

        public int getProductId()
        {
            return productId;
        }
        public string getName()
        {
            return name;
        }
        public string getType()
        {
            return type;
        }
        public int getLocation()
        {
            return location;
        }
        public bool getInStock()
        {
            return inStock;
        }
        public int getStockCount()
        {
            return stockCount;
        }
        public double getPrice()
        {
            return price;
        }
    }
}
